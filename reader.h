#ifndef READER_H
#define READER_H

#include "core.h"

obj_t* tokenize(char_t* str);
char_t* read_file(const char_t* filename);
obj_t* parse_list(obj_t** p_rest_tokens);
obj_t* parse(obj_t* tokens);

#endif /* READER_H */
