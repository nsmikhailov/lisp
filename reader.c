#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "reader.h"
#include "core.h"
#include "syntax.h"

size_t token_len(obj_t* token) {
    return string_len(token);
}

obj_t* token_copy(obj_t* token) {
    return string_copy(token);
}

char_t* ncopy(const char_t* str, size_t n) {
    char_t* new = (char_t*) malloc((n + 1) * sizeof(char_t));
    size_t i = 0;
    for (; i < n; ++i) {
        new[i] = str[i];
    }
    new[n] = '\0';
    return new;
}

obj_t* push_as_token(obj_t* list, const char_t* str, size_t n) {
    char_t* cur_token = ncopy(str, n);
    obj_t* nl = cons(string(cur_token), list);
    free(cur_token);
    return nl;
}

obj_t* tokenize(char_t* str) {
    size_t i = 0;
    size_t first = 0;
    size_t len = strlen(str);
    size_t cur_len = 0;
    /*obj_t* cur_token = string(""); */
    obj_t* tokens = nil;
    char_t* str_last = NULL;
    for (; i < len; ++i) {
        switch (str[i]) {
            case '(':
            case ')':
            case '\'':
                cur_len = i - first;
                if (cur_len > 0) {
                    tokens = push_as_token(tokens, &str[first], cur_len);
                }
                tokens = push_as_token(tokens, &str[i], 1);
                first = i + 1;
                break;
            case ' ':
            case '\n':
            case '\t':
            case '\b':
                cur_len = i - first;
                if (cur_len > 0) {
                    tokens = push_as_token(tokens, &str[first], cur_len);
                }
                first = i + 1;
                break;
            case '"':
                str_last = strchr(&str[i+1], '"');
                if (str_last == NULL) {
                    fprintf(stderr, "syntax error\n");
                    exit(1);
                }
                cur_len = ((str_last - &str[i]) / sizeof(char_t)) + 1;
                tokens = push_as_token(tokens, &str[first], cur_len);
                first = i + cur_len;
                i = first - 1;
                break;
            default:
                break;
        };
    }
    return reversed(tokens);
}

char_t* read_file(const char_t* filename) {
    #define BUF_SIZE 4096
    char_t* str = NULL;
    const size_t buf_size = BUF_SIZE;
    size_t read_num = 0;
    size_t total_read_num = 0;

    char_t buffer[BUF_SIZE + 1] = { '\0' };
    #undef BUF_SIZE

    FILE* fd = NULL;
    char_t* tmp = NULL;

    buffer[buf_size] = '\0';

    if ((fd = fopen(filename, "r"))) {
        while (!feof(fd)) {
            read_num = fread(buffer, sizeof(char_t), buf_size, fd);
            if (read_num == 0) break;
            buffer[read_num] = '\0';
            total_read_num += read_num;
            tmp = str;
            str = (char_t *)malloc((total_read_num + 1) * sizeof(char_t));
            str[0] = '\0';
            if (tmp != NULL) strcpy(str, tmp);
            strcat(str, buffer);
            free(tmp);
        }
        fclose(fd);
    }
    else {
        fprintf(stderr, "Can\'t open file %s\n", filename);
        exit(1);
    }
    return str;
}

bool_t is_list_begin(const char_t* token) {
    return strcmp(token, LIST_BEGIN) == 0;
}
bool_t is_list_end(const char_t* token) {
    return strcmp(token, LIST_END) == 0;
}

bool_t match_string(const char_t* token) {
    return (token[0] == '"') && (token[strlen(token)-1] == '"');
}

obj_t* parse_list(obj_t** p_rest_tokens) {
    obj_t* rest_tokens = *p_rest_tokens;
    obj_t* parsed = nil;
    char_t* cur_token = string_value(car(rest_tokens));
    obj_t* elem = NULL;
    char_t* tmp_str = NULL;
    size_t tmp_str_len = 0;
    /* It's not a list */
    assert(is_list_begin(cur_token));
    /* Discard opening parenthesis */
    rest_tokens = cdr(rest_tokens), cur_token = string_value(car(rest_tokens));
    for (; !is_null(rest_tokens) && !is_list_end(cur_token);
           (rest_tokens = cdr(rest_tokens),
            cur_token = string_value(car(rest_tokens)))) {
        if (is_list_begin(cur_token)) {
            elem = parse_list(&rest_tokens);
            if (is_null(rest_tokens)) {
                parsed = cons(elem, parsed);
                goto return_result;
            }
        }
        else if (match_number(cur_token)) {
            elem = number(cur_token);
        }
        else if (match_string(cur_token)) {
            tmp_str_len = strlen(cur_token);
            tmp_str = malloc((tmp_str_len - 1) * sizeof(char_t));
            tmp_str[tmp_str_len - 2] = '\0';
            strncpy(tmp_str, cur_token + 1, tmp_str_len - 2);
            elem = string(tmp_str);
            free(tmp_str);
        }
        else {
            elem = symbol(cur_token);
        }
        parsed = cons(elem, parsed);
    }
    if (is_null(rest_tokens)) {
        fprintf(stderr, "List is not complete");
        exit(1);
    }
    /* Do not discard closing parenthesis, because for loop discards It
     * automatically after return */
return_result:
    *p_rest_tokens = rest_tokens;
    return reversed(parsed);
}

obj_t* parse(obj_t* tokens) {
    obj_t *c = tokens;
    obj_t *ol = nil;
    char_t *token = NULL;
    for (; !is_null(c); c = cdr(c)) {
        token = string_value(car(c));
        if (strcmp(token, "(") == 0) {
            c = cdr(c);
            token = string_value(car(c));
            ol = cons(parse_list(&c), ol);
            if (is_null(c)) return reversed(ol);
        }
        else if (match_number(token)) {
            ol = cons(number(token), ol);
        }
        else {
            ol = cons(symbol(token), ol);
        }
    }
    return reversed(ol);
}

