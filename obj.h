#ifndef OBJ_H
#define OBJ_H

#include "ctypes.h"

enum base_enum { PAIR, SYMBOL, TYPED };
typedef enum base_enum base_t;

typedef struct {
    base_t type;
    size_t size;
    anon_t *value;
} obj_t;

obj_t* obj_make(base_t type, size_t size, anon_t *value);

#endif /* OBJ_H */
