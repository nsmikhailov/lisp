(begin
  (define Y
    (fn (f)
        ((fn (x) (f (fn (v) ((x x) v))))
         (fn (x) (f (fn (v) ((x x) v)))))))
  (define fact
    (Y (fn (f)
           (fn (n)
               (if (== n 0)
                 1
                 (* n (f (- n 1))))))))
  (print (fact 10))
)
