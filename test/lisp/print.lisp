(begin
  (print "hello")
  (print (quote hello))
  (print 1)
  (print 2.0)
  (print (cons 1 2))
  (print (cons 1 (cons 2 3)))
  (print (cons 1 (cons 2 nil))))
