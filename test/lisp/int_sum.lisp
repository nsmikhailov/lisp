(begin
  (define (assert val msg)
    (if val (quote ok) (print msg)))
  (assert (== (+ 10 5 15) 30) "Failed: not (== (+ 10 5 15) 30)"))
