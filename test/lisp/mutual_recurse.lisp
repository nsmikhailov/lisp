(begin
  (define (odd? n)
    (if (== n 0)
      'true
      (even? (- n 1))))
  (define (even? n)
    (if (== n 0)
      'false
      (odd? (- n 1))))
  (print (odd? 10))
  (print (even? 10)))
