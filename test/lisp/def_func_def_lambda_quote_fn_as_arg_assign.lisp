(begin
  (define (a x y) (- x y))
  (define run (fn (proc x y) (proc x y) (= a 2)))
  (run a (quote 100) 10)
  (define b 100)
  (= b 101))

