#ifndef NUMBER_H
#define NUMBER_H

#include "ctypes.h"
#include "obj.h"

bool_t is_number(const obj_t* obj);
bool_t is_real(const obj_t* obj);
bool_t is_ratio(const obj_t* obj);

/* IO */
obj_t* number(const char_t* str);
bool_t match_number(const char_t* str);
empty_t print_number(const obj_t* obj);

obj_t* number_mul(const obj_t* num1, const obj_t* num2);
obj_t* number_div(const obj_t* num1, const obj_t* num2);
obj_t* number_sum(const obj_t* num1, const obj_t* num2);
obj_t* number_sub(const obj_t* num1, const obj_t* num2);

int number_cmp(const obj_t* num1, const obj_t* num2);

#endif /* NUMBER_H */
