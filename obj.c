#include <stdlib.h>

#include "obj.h"

obj_t* obj_make(base_t type, size_t size, anon_t *value) {
    obj_t* obj = (obj_t *)malloc(sizeof(obj_t));
    obj->type = type;
    obj->size = size;
    obj->value = value;
    return obj;
}

