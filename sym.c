#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "sym.h"
#include "pair.h"

static obj_t* interned_symbols = NULL;

void intern_symbol(obj_t* sym) {
    interned_symbols = cons(sym, interned_symbols);
}

obj_t* new_symbol(const char_t* name) {
    size_t size = (strlen(name) + 1) * sizeof(char_t);
    char_t *value = (char_t *)malloc(size);
    strcpy(value, name);
    return obj_make(SYMBOL, size, (anon_t *)value);
}

obj_t* find_symbol(const char_t* name) {
    obj_t* p = interned_symbols;
    for (; p != NULL; p = cdr(p)) {
        if (is_symbol_name(car(p), name)) {
            return car(p);
        }
    };
    return NULL;
}

obj_t* symbol(const char_t* name) {
    obj_t* sym = find_symbol(name);
    if (sym == NULL) {
        sym = new_symbol(name);
        intern_symbol(sym);
    }
    return sym;
}

bool_t is_symbol(obj_t* obj) {
    return obj->type == SYMBOL;
}

bool_t is_symbol_name(obj_t* obj, const char_t* name) {
    return strcmp(symbol_name(obj), name) == 0;
}

bool_t is_same_symbol(const obj_t* sym1, const obj_t* sym2) {
    return sym1 == sym2;
}

const char_t* symbol_name(obj_t* obj) {
    assert(is_symbol(obj));
    return (char_t *)obj->value;
}

bool_t is_null(obj_t* obj) {
    return is_same_symbol(obj, nil);
}

