#ifndef RUNTIME_H
#define RUNTIME_H

#include "core.h"
#include "env.h"

bool_t is_self_evaluating(obj_t* expr);
bool_t is_quoted(obj_t* expr);
bool_t is_variable(obj_t* expr);
bool_t is_assignment(obj_t* expr);
bool_t is_definition(obj_t* expr);
bool_t is_lambda_expr(obj_t* expr);
bool_t is_if(obj_t* expr);
bool_t is_begin(obj_t* expr);
bool_t is_application(obj_t* expr);
obj_t* list_of_values(obj_t* exprs, env_t env);
obj_t* eval_if(obj_t* expr, env_t env);
obj_t* eval_sequence(obj_t* exprs, env_t env);
obj_t* make_lambda(obj_t* parameters, obj_t* body);
/* assign */
obj_t* assignment_variable(obj_t* expr);
obj_t* assignment_value(obj_t* expr);
obj_t* eval_assignment(obj_t* expr, env_t env);
/* end assign */
/* define */
obj_t* definition_variable(obj_t* expr);
obj_t* definition_value(obj_t* expr);
obj_t* eval_definition(obj_t* expr, env_t env);
/* end define */
obj_t* text_of_quotation(obj_t* expr);
/*
obj_t* lambda_parameters(obj_t* expr);
obj_t* lambda_body(obj_t* expr);
*/
obj_t* if_predicate(obj_t* expr);
obj_t* if_consequent(obj_t* expr);
obj_t* if_alternative(obj_t* expr);
obj_t* make_if(obj_t* predicate, obj_t* consequent, obj_t* alternative);
obj_t* begin_actions(obj_t* expr);
bool_t is_last_expr(obj_t* seq);
obj_t* first_expr(obj_t* seq);
obj_t* rest_exprs(obj_t* seq);
obj_t* sequence_to_expr(obj_t* seq);
obj_t* make_begin(obj_t* seq);
obj_t* operator(obj_t* expr);
obj_t* operands(obj_t* expr);
bool_t is_no_operands(obj_t* ops);
obj_t* first_operand(obj_t* ops);
obj_t* rest_operands(obj_t* ops);
bool_t is_true(obj_t* expr);
bool_t is_false(obj_t* expr);
obj_t* make_procedure(obj_t* parameters, obj_t* body, env_t env);
bool_t is_compound_procedure(obj_t* p);
obj_t* procedure_parameters(obj_t* p);
obj_t* procedure_body(obj_t* p);
env_t procedure_environment(obj_t* p);
obj_t* apply(obj_t* procedure, obj_t* arguments);
obj_t* eval(obj_t* expr, env_t env);
bool_t is_tagged_list(obj_t* expr, const char_t* tag);
bool_t not(bool_t v);

#endif /* CORE_H */
