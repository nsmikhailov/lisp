#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "pair.h"
#include "env.h"

bool_t is_same_variable(obj_t* var1, obj_t* var2) {
    return is_same_symbol(var1, var2);
}

env_t enclosing_env(env_t env) {
    return cdr(env);
}

obj_t* make_frame(obj_t* variables, obj_t* values, env_t enclosing) {
    return cons(cons(variables, values), enclosing);
}

obj_t* first_frame(env_t env) {
    return car(env);
}

obj_t* frame_variables(obj_t* frame) {
    return car(frame);
}

obj_t* frame_values(obj_t* frame) {
    return cdr(frame);
}

obj_t* lookup_var_value(obj_t* var, env_t env) {
    obj_t *frame, *vars, *vals;
    bool_t after_dot = false;
    /* No such variable */
    if (is_null(env)) {
        fprintf(stderr, "No such variable in current context: %s\n", symbol_name(var));
        exit(1);
    }
    frame = first_frame(env);
    vars = frame_variables(frame);
    vals = frame_values(frame);
    for (; !is_null(vars); vars = cdr(vars)) {
        if (is_symbol_name(car(vars), "&rest")) {
            vars = cdr(vars);
            after_dot = true;
        }
        if (is_same_variable(var, car(vars))) {
            if (after_dot) return vals;
            if (is_null(vals)) {
                fprintf(stderr, "Value was not specified for %s\n", symbol_name(var));
                exit(1);
            }
            return car(vals);
        }
        if (!is_null(vals)) vals = cdr(vals);
    }
    return lookup_var_value(var, enclosing_env(env));
}

empty_t set_var_value(obj_t* var, obj_t* val, env_t env) {
    obj_t *frame, *vars, *vals;
    /* No such variable */
    assert(!is_null(env));
    frame = first_frame(env);
    vars = frame_variables(frame);
    vals = frame_values(frame);
    for (; !is_null(vars); vars = cdr(vars), vals = cdr(vals)) {
        if (is_same_variable(var, car(vars))) {
            set_car(vals, val);
            return;
        }
    }
    set_var_value(var, val, enclosing_env(env));
}

empty_t define_var(obj_t* var, obj_t* val, env_t env) {
    obj_t* frame = first_frame(env);
    set_car(frame, cons(var, car(frame)));
    set_cdr(frame, cons(val, cdr(frame)));
}

env_t extend_env(obj_t* variables, obj_t* values, env_t env) {
    return make_frame(variables, values, env);
}

