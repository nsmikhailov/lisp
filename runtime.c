#include <stdlib.h>
#include <stdio.h>

#include "runtime.h"
#include "core.h"
#include "env.h"
#include "syntax.h"
#include "reader.h"

bool_t is_self_evaluating(obj_t* expr) {
    return is_number(expr) || is_string(expr);
}

bool_t is_variable(obj_t* expr) {
    return is_symbol(expr);
}

bool_t is_tagged_list(obj_t* expr, const char_t* tag) {
    return is_pair(expr) && is_symbol(car(expr)) && is_symbol_name(car(expr), tag);
}

bool_t is_quoted(obj_t* expr) {
    return is_tagged_list(expr, QUOTE);
}

bool_t is_assignment(obj_t* expr) {
    return is_tagged_list(expr, ASSIGN);
}

bool_t is_definition(obj_t* expr) {
    return is_tagged_list(expr, DEFINE);
}

bool_t is_lambda_expr(obj_t* expr) {
    return is_tagged_list(expr, LAMBDA);
}

bool_t is_if(obj_t* expr) {
    return is_tagged_list(expr, IF);
}

bool_t is_begin(obj_t* expr) {
    return is_tagged_list(expr, PROGN);
}

bool_t is_application(obj_t* expr) {
    return is_pair(expr);
}

obj_t* list_of_values(obj_t* exprs, env_t env) {
    if (is_no_operands(exprs)) {
        return nil;
    }
    else {
        return cons(eval(first_operand(exprs), env),
                    list_of_values(rest_operands(exprs), env));
    }
}

obj_t* eval_if(obj_t* expr, env_t env) {
    if (is_true(eval(if_predicate(expr), env))) {
        return eval(if_consequent(expr), env);
    }
    else {
        return eval(if_alternative(expr), env);
    }
}

obj_t* eval_sequence(obj_t* exprs, env_t env) {
    if (is_last_expr(exprs)) {
        return eval(first_expr(exprs), env);
    }
    else {
        eval(first_expr(exprs), env);
        return eval_sequence(rest_exprs(exprs), env);
    }
}

obj_t* make_lambda(obj_t* parameters, obj_t* body) {
    return cons(symbol(LAMBDA), cons(parameters, body));
}


/* assign */
obj_t* assignment_variable(obj_t* expr) {
    return car(cdr(expr));
}

obj_t* assignment_value(obj_t* expr) {
    return car(cdr(cdr(expr)));
}

obj_t* eval_assignment(obj_t* expr, env_t env) {
    set_var_value(assignment_variable(expr),
                       eval(assignment_value(expr), env),
                       env);
    return symbol("ok");
}
/* end assign */

/* define */
obj_t* definition_variable(obj_t* expr) {
    if (is_symbol(car(cdr(expr)))) {
        return car(cdr(expr));
    }
    else {
        return car(car(cdr(expr)));
    }
}

obj_t* definition_value(obj_t* expr) {
    if (is_symbol(car(cdr(expr)))) {
        return car(cdr(cdr(expr)));
    }
    else {
        return make_lambda(cdr(car(cdr(expr))), cdr(cdr(expr)));
    }
}

obj_t* eval_definition(obj_t* expr, env_t env) {
    define_var(definition_variable(expr),
               eval(definition_value(expr), env),
               env);
    return symbol("ok");
}
/* end define */

obj_t* text_of_quotation(obj_t* expr) {
    return car(cdr(expr));
}

obj_t* lambda_expr_params(obj_t* expr) {
    return car(cdr(expr));
}

obj_t* lambda_expr_body(obj_t* expr) {
    return cdr(cdr(expr));
}

obj_t* if_predicate(obj_t* expr) {
    return car(cdr(expr));
}

obj_t* if_consequent(obj_t* expr) {
    return car(cdr(cdr(expr)));
}

obj_t* if_alternative(obj_t* expr) {
    if (not(is_null(cdr(cdr(cdr(expr)))))) {
        return car(cdr(cdr(cdr(expr))));
    }
    else {
        return nil;
    }
}

obj_t* make_if(obj_t* predicate, obj_t* consequent, obj_t* alternative) {
    return cons(symbol(IF),
                cons(predicate,
                     cons(consequent,
                          cons(alternative, nil))));
}

obj_t* begin_actions(obj_t* expr) {
    return cdr(expr);
}

bool_t is_last_expr(obj_t* seq) {
    return is_null(cdr(seq));
}

obj_t* first_expr(obj_t* seq) {
    return car(seq);
}

obj_t* rest_exprs(obj_t* seq) {
    return cdr(seq);
}

obj_t* sequence_to_expr(obj_t* seq) {
    if (is_null(seq)) {
        return seq;
    }
    else if (is_last_expr(seq)) {
        return first_expr(seq);
    }
    else {
        return make_begin(seq);
    }
}

obj_t* make_begin(obj_t* seq) {
    return cons(symbol(PROGN), seq);
}

obj_t* operator(obj_t* expr) {
    return car(expr);
}

obj_t* operands(obj_t* expr) {
    return cdr(expr);
}

bool_t is_no_operands(obj_t* ops) {
    return is_null(ops);
}

obj_t* first_operand(obj_t* ops) {
    return car(ops);
}

obj_t* rest_operands(obj_t* ops) {
    return cdr(ops);
}

bool_t is_true(obj_t* expr) {
    return !is_null(expr);
}

bool_t is_false(obj_t* expr) {
    return is_null(expr);
}

obj_t* make_procedure(obj_t* parameters, obj_t* body, env_t env) {
    return cons(symbol("procedure"), cons((anon_t*)parameters, cons(body, cons((anon_t*)env, nil))));
    /*return list(4, symbol("procedure"), parameters, body, env);*/
}

bool_t is_compound_procedure(obj_t* p) {
    return is_tagged_list(p, "procedure");
}

obj_t* procedure_parameters(obj_t* p) {
    return (obj_t*) car(cdr(p));
}

obj_t* procedure_body(obj_t* p) {
    return car(cdr(cdr(p)));
}

env_t procedure_environment(obj_t* p) {
    return (env_t) car(cdr(cdr(cdr(p))));
}

bool_t is_load(obj_t* expr) {
    return is_tagged_list(expr, LOAD);
}

obj_t* eval_load(obj_t* expr, env_t env) {
    char_t* filename = string_value(car(cdr(expr)));
    obj_t* tokens = tokenize(read_file(filename));
    obj_t* parsed = parse_list(&tokens);
    return eval(parsed, env);
}

obj_t* eval(obj_t* expr, env_t env) {
    if (is_self_evaluating(expr)) {
        return expr;
    }
    else if (is_if(expr)) {
        return eval_if(expr, env);
    }
    else if (is_begin(expr)) {
        return eval_sequence(begin_actions(expr), env);
    }
    else if (is_load(expr)) {
        return eval_load(expr, env);
    }
    else if (is_variable(expr)) {
        return lookup_var_value(expr, env);
    }
    else if (is_definition(expr)) {
        return eval_definition(expr, env);
    }
    else if (is_lambda_expr(expr)) {
        return make_procedure(lambda_expr_params(expr), lambda_expr_body(expr), env);
    }
    else if (is_quoted(expr)) {
        return text_of_quotation(expr);
    }
    else if (is_assignment(expr)) {
        return eval_assignment(expr, env);
    }
    else if (is_application(expr)) {
        return apply(eval(operator(expr), env),
                     list_of_values(operands(expr), env));
    }
    else {
        fprintf(stderr, "eval: Unknown expression type\n");
        exit(1);
    }
}

obj_t* apply(obj_t* procedure, obj_t* arguments) {
    if (is_primitive(procedure)) {
        return apply_primitive(procedure, arguments);
    }
    else if (is_compound_procedure(procedure)) {
        return eval_sequence(procedure_body(procedure),
                             extend_env(
                                 procedure_parameters(procedure),
                                 arguments,
                                 procedure_environment(procedure)));
    }
    else {
        fprintf(stderr, "apply: Unknown procedure type\n");
        exit(1);
    }
}

bool_t not(bool_t v) {
    return !v;
}

