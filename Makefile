CC=cc
#CFLAGS=-std=c11 -O3
CFLAGS=-ggdb -std=c90 -pedantic -Wall -Wextra -Werror -lgmp
LDFLAGS=-c

#HEADERS=reader.h list.h types.h core.h environment.h error.h proc.h \
#		built_in.h symbol.h obj.h sym.h pair.h typed.h
HEADERS=obj.h sym.h pair.h typed.h number.h string.h proc.h env.h reader.h \
		runtime.h built_in.h
SOURCES=$(HEADERS:.h=.c) lisp.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=lisp

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(CFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(LDFLAGS) $(CFLAGS) $< -o $@

all: $(SOURCES) $(EXECUTABLE)

clean:
	rm *.o
	rm $(EXECUTABLE)

.PHONY: all

