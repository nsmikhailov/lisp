#include <assert.h>
#include <stdlib.h>

#include "pair.h"
#include "typed.h"

typedef struct {
    obj_t* car;
    obj_t* cdr;
} pair_t;

pair_t* extract_pair(obj_t* obj) {
    assert(is_pair(obj));
    return (pair_t *)(obj->value);
}

obj_t* car(obj_t* pair) {
    return extract_pair(pair)->car;
}

obj_t* cdr(obj_t* pair) {
    return extract_pair(pair)->cdr;
}

bool_t is_pair(obj_t* obj) {
    return obj->type == PAIR;
}

obj_t* cons(obj_t* car, obj_t* cdr) {
    size_t size = sizeof(pair_t);
    pair_t* pair = (pair_t *)malloc(size);
    pair->car = car;
    pair->cdr = cdr;
    return obj_make(PAIR, size, (anon_t *)pair);
}

empty_t set_car(obj_t* pair, obj_t* value) {
    extract_pair(pair)->car = value;
}

empty_t set_cdr(obj_t* pair, obj_t* value) {
    extract_pair(pair)->cdr = value;
}

obj_t* reversed(obj_t* obj) {
    obj_t* rev = nil;
    while (!is_null(obj)) {
        rev = cons(car(obj), rev);
        obj = cdr(obj);
    }
    return rev;
}

size_t list_len(obj_t* obj) {
    size_t len = 0;
    for(; !is_null(obj); obj = cdr(obj), ++len) ;
    return len;
}
