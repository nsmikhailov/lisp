#include <assert.h>
#include <stdlib.h>

#include "typed.h"
#include "sym.h"

typedef struct {
    obj_t* type;
    anon_t* value;
} typed_t;

obj_t* typed(obj_t* type, anon_t* value) {
    size_t size = sizeof(typed_t);
    typed_t* obj = (typed_t *) malloc(size);
    obj->type = type;
    obj->value = value;
    return obj_make(TYPED, size, (anon_t *) obj);
}

bool_t is_typed(const obj_t* obj) {
    return obj->type == TYPED;
}

typed_t* extract_typed(const obj_t* obj) {
    assert(is_typed(obj));
    return (typed_t *) (obj->value);
}

obj_t* type_of(const obj_t* obj) {
    return extract_typed(obj)->type;
}

anon_t* value_of(const obj_t* obj) {
    return extract_typed(obj)->value;
}

bool_t is_instance_of(const obj_t* obj, const obj_t* type) {
    return is_typed(obj) && is_same_symbol(type_of(obj), type);
}

