#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>

#include "number.h"
#include "typed.h"
#include "sym.h"

#define TT_REAL (symbol("real"))
#define TT_RATIO (symbol("ratio"))

real_t real_value_of(const obj_t* obj) {
    if (is_real(obj)) {
        return *((real_t*) value_of(obj));
    }
    else if (is_ratio(obj)) {
        return mpq_get_d(*((mpq_t*) value_of(obj)));
    }
    assert(0);
}

mpq_t* ratio_value_of(const obj_t* obj) {
    assert(is_ratio(obj));
    return (mpq_t*) value_of(obj);
}

bool_t match_real(const char_t* str) {
    size_t len = strlen(str);
    size_t i;
    size_t dots_count = 0;
    bool_t sign;
    if (len == 0)
        return false;
    sign = (str[0] == '+' || str[0] == '-');
    if (sign && (len == 1 || len == 2))
        return false;
    for (i = (sign ? 1 : 0); i < len; ++i) {
        if (str[i] == '.') {
            ++dots_count;
            continue;
        }
        if (!isdigit(str[i]))
            return false;
    };
    return (dots_count == 1);
}

bool_t match_ratio(const char_t* str) {
    size_t len = strlen(str);
    size_t i, before = 0, after = 0;
    bool_t sign, has_div = false;

    if (len == 0)
        return false;

    sign = (str[0] == '+' || str[0] == '-');
    if (sign && len == 1)
        return false;

    for (i = (sign ? 1 : 0); i < len; ++i) {
        if (str[i] == '/') {
            if (has_div)
                return false;
            has_div = true;
            continue;
        }

        if (!isdigit(str[i]))
            return false;
        if (has_div) {
            after++;
        }
        else {
            before++;
        }
    }
    if (has_div) {
        return after > 0 && before > 0;
    }
    else {
        return true;
    }
}

bool_t match_number(const char_t* str) {
    return match_ratio(str) || match_real(str);
}

obj_t* number(const char_t* str) {
    if (match_real(str)) {
        real_t* val = (real_t*) malloc(sizeof(real_t));
        *val = atof(str);

        return typed(TT_REAL, val);
    }
    else if (match_ratio(str)) {
        mpq_t* val = (mpq_t*) malloc(sizeof(mpq_t));
        mpq_init(*val);
        mpq_set_str(*val, str, 10);

        return typed(TT_RATIO, val);
    }
    assert(0);
}

bool_t is_real(const obj_t* obj) {
    return is_instance_of(obj, TT_REAL);
}

bool_t is_ratio(const obj_t* obj) {
    return is_instance_of(obj, TT_RATIO);
}

bool_t is_number(const obj_t* obj) {
    return is_real(obj) || is_ratio(obj);
}

empty_t print_number(const obj_t* obj) {
    if (is_instance_of(obj, TT_REAL)) {
        printf("%g", real_value_of(obj));
    }
    else if (is_instance_of(obj, TT_RATIO)) {
        gmp_printf("%Qd", ratio_value_of(obj));
    }
    else {
        assert(0);
    }
}

#define ARITHMETICAL(NAME, OP_REAL, OP_RATIO)\
obj_t* NAME(const obj_t* num1, const obj_t* num2) {\
    if (is_real(num1) || is_real(num2)) {\
        real_t* val = (real_t*) malloc(sizeof(real_t));\
        *val = real_value_of(num1) OP_REAL real_value_of(num1);\
\
        return typed(TT_REAL, val);\
    } else {\
        mpq_t* val = (mpq_t*) malloc(sizeof(mpq_t));\
        mpq_init(*val);\
        OP_RATIO(*val, *ratio_value_of(num1), *ratio_value_of(num2));\
        return typed(TT_RATIO, val);\
    }\
}

ARITHMETICAL(number_mul, *, mpq_mul)
ARITHMETICAL(number_div, /, mpq_div)
ARITHMETICAL(number_sum, +, mpq_add)
ARITHMETICAL(number_sub, -, mpq_sub)

#undef ARITHMETICAL

int number_cmp(const obj_t* num1, const obj_t* num2) {
    if (is_real(num1) || is_real(num2)) {
        if (real_value_of(num1) < real_value_of(num2))
            return -1;
        else if (real_value_of(num1) > real_value_of(num2))
            return 1;
        return 0;
    }
    else {
        mpq_t* val = (mpq_t*) malloc(sizeof(mpq_t));
        mpq_init(*val);
        return mpq_cmp(*ratio_value_of(num1), *ratio_value_of(num2));
    }
}
