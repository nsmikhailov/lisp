#ifndef BUILT_IN_H
#define BUILT_IN_H

#include "obj.h"
#include "env.h"

obj_t* apply_sum(obj_t* args);
obj_t* apply_sub(obj_t* args);
obj_t* apply_mul(obj_t* args);
obj_t* apply_div(obj_t* args);

obj_t* apply_cons(obj_t* args);
empty_t print_atom(obj_t* a);
empty_t print_list(obj_t* l);
empty_t print(obj_t* obj);
obj_t* apply_print(obj_t* obj);
/*
obj_t* apply_div(obj_t* args);
*/
env_t setup_env();

#endif /* BUILT_IN_H */
