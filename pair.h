#ifndef PAIR_H
#define PAIR_H

#include "ctypes.h"
#include "obj.h"

obj_t* car(obj_t* pair);
obj_t* cdr(obj_t* pair);
empty_t set_car(obj_t* pair, obj_t* value);
empty_t set_cdr(obj_t* pair, obj_t* value);
bool_t is_pair(obj_t* obj);
obj_t* cons(obj_t* car, obj_t* cdr);
/* reverse list, only nil terminated */
obj_t* reversed(obj_t* obj);
size_t list_len(obj_t* obj);

#include "sym.h"
#define TT_PAIR (symbol("pair"))

#endif /* PAIR_H */
