#ifndef SYM_H
#define SYM_H

#include "ctypes.h"
#include "obj.h"

obj_t* symbol(const char_t* name);
bool_t is_symbol(obj_t* obj);
bool_t is_same_symbol(const obj_t* sym1, const obj_t* sym2);
bool_t is_symbol_name(obj_t* sym, const char_t* name);
const char_t* symbol_name(obj_t* obj);
bool_t is_null(obj_t* obj);

#define nil (symbol("nil"))

#endif /* SYM_H */
