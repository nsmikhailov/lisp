#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <string.h>

#include "core.h"
#include "env.h"
#include "built_in.h"
#include "syntax.h"
#include "number.h"

empty_t print_atom(obj_t* a) {
    if (is_string(a)) {
        printf("\"%s\"", string_value(a));
    }
    else if (is_number(a)) {
        print_number(a);
    }
    else if (is_symbol(a)) {
        printf("%s", symbol_name(a));
    }
}

empty_t print_list(obj_t* l) {
    printf(LIST_BEGIN);
    for (;!is_null(l); l = cdr(l)) {
        obj_t* h = car(l);
        if (is_pair(h)) {
            print_list(h);
        }
        else {
            print_atom(h);
        }
        if (!is_null(cdr(l))) {
            printf(" ");
            if (!is_pair(cdr(l))) {
                printf(". ");
                print_atom(cdr(l));
                break;
            }
        }
    }
    printf(LIST_END);
}

empty_t print(obj_t* obj) {
    if (is_pair(obj)) {
        print_list(obj);
    }
    else {
        print_atom(obj);
    }
    printf("\n");
}

#define ARITHMETICAL(NAME, OP)\
obj_t* NAME(obj_t* args) {\
    obj_t* res, *p;\
    assert(is_number(car(args)));\
\
    res = car(args);\
    p = cdr(args);\
\
    for (; !is_null(p); p = cdr(p)) {\
        assert(is_number(car(p)));\
        res = OP(res, car(p));\
    }\
    return res;\
}

ARITHMETICAL(apply_sum, number_sum)
ARITHMETICAL(apply_mul, number_mul)
ARITHMETICAL(apply_div, number_div)
ARITHMETICAL(apply_sub, number_sub)

#undef ARITHMETICAL

obj_t* apply_equal(obj_t* args) {
    obj_t *p, *cur, *next;
    bool_t eq = true;
    assert(list_len(args) > 1);
    /*if (args_num < 2) error_fatal("apply_equal: too few arguments");*/
    for (p = args; !is_null(cdr(p)); p = cdr(p)) {
        cur = car(p);
        next = car(cdr(p));
        if (cur->type == next->type) {
            if (is_typed(cur) && is_typed(next)) {
                eq &= (type_of(cur) == type_of(next));
                if (!eq)
                    return nil;
            }
        }
        else {
            return nil;
        }
        if (is_number(cur) && is_number(next)) {
            eq &= (number_cmp(cur, next) == 0);
            if (!eq)
                return nil;
        }
        else if (is_string(cur) && is_string(next)) {
            eq &= is_equal_strings(cur, next);
            if (!eq)
                return nil;
        }
        else if (is_symbol(cur) && is_symbol(next)) {
            eq &= is_same_symbol(cur, next);
            if (!eq)
                return nil;
        }
        else {
            return nil;
        }
    }
    return symbol(TRUE);
}

obj_t* apply_cons(obj_t* args) {
    assert(list_len(args) == 2);
    return cons(car(args), car(cdr(args)));
}

obj_t* apply_print(obj_t* obj) {
    print(car(obj));
    return nil;
}

obj_t* apply_car(obj_t* obj) {
    obj_t *inner = car(obj);
    return is_null(inner) ? nil : car(inner);
}

obj_t* apply_cdr(obj_t* obj) {
    obj_t *inner = car(obj);
    return is_null(inner) ? nil : cdr(inner);
}

obj_t *apply_type_of(obj_t *args) {
    obj_t *obj = car(args);
    switch (obj->type) {
        case PAIR:
            return symbol("pair");
        case SYMBOL:
            return symbol("symbol");
        case TYPED:
            return type_of(obj);
        default:
            assert(0);
    };
}

obj_t *apply_string_to_list(obj_t *args) {
    obj_t *s = car(args);
    char_t *str;
    obj_t *list = nil;
    size_t i;
    size_t str_len;
    char_t sym_name[2];
    sym_name[1] = '\0';

    assert(is_string(s));

    str = string_value(s);
    str_len = strlen(str);

    for (i = 0; i < str_len; ++i) {
        sym_name[0] = str[i];
        list = cons(string(sym_name), list);
    }
    return reversed(list);
}

obj_t *apply_list_to_string(obj_t *args) {
    obj_t *list = car(args);
    obj_t *p, *cur;
    char_t *str = NULL, *add, *old_str;
    size_t add_len = 0, old_len = 0;
    for (p = list; !is_null(p); p = cdr(p)) {
        cur = car(p);
        add = string_value(cur);
        add_len = strlen(add);
        old_str = str;
        str = (char_t *)malloc(sizeof(char_t) * (old_len + add_len + 1));
        old_len = old_len + add_len;
        if (old_str != NULL) {
            strcpy(str, old_str);
            free(old_str);
        }
        strcat(str, add);
    }
    return string(str);
}

env_t setup_env() {
    env_t env = the_empty_env;
    define_var(symbol("+"), primitive(apply_sum), env);
    define_var(symbol("-"), primitive(apply_sub), env);
    define_var(symbol("/"), primitive(apply_div), env);
    define_var(symbol("*"), primitive(apply_mul), env);
    define_var(symbol("=="), primitive(apply_equal), env);
    define_var(symbol("cons"), primitive(apply_cons), env);
    define_var(symbol("print"), primitive(apply_print), env);
    define_var(symbol("car"), primitive(apply_car), env);
    define_var(symbol("cdr"), primitive(apply_cdr), env);
    define_var(symbol("type-of"), primitive(apply_type_of), env);
    define_var(symbol("str->list"), primitive(apply_string_to_list), env);
    define_var(symbol("list->str"), primitive(apply_list_to_string), env);
    define_var(symbol(TRUE), symbol(TRUE), env);
    define_var(symbol(NIL), symbol(NIL), env);
    define_var(symbol(LAMBDA), symbol(LAMBDA), env);
    return env;
}

