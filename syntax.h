#ifndef SYNTAX_H
#define SYNTAX_H

#define LAMBDA "fn"
#define TRUE "t"
#define QUOTE "quote"
#define ASSIGN "="
#define DEFINE "define"
#define PROGN "begin"
#define IF "if"
#define LIST_BEGIN "("
#define LIST_END ")"
#define LOAD "load"
#define NIL "nil"

#endif /* SYNTAX_H */
