#include <string.h>
#include <stdlib.h>

#include "typed.h"
#include "string.h"

obj_t* string(const char_t* value) {
    size_t size = sizeof(char_t) * (strlen(value) + 1);
    char_t* copy = (char_t *)malloc(size);
    strcpy(copy, value);
    return typed(TT_STRING, copy);
}

bool_t is_string(obj_t* obj) {
    return is_instance_of(obj, TT_STRING);
}

char_t* string_value(obj_t* obj) {
    return (char_t *)value_of(obj);
}

obj_t* string_copy(obj_t* str) {
    return string(string_value(str));
}

size_t string_len(obj_t* str) {
    return strlen(string_value(str));
}

bool_t is_equal_strings(obj_t* str1, obj_t* str2) {
    return (strcmp(string_value(str1), string_value(str2)) == 0);
}

bool_t is_string_content_equal(obj_t* str, const char_t* content) {
    return (strcmp(string_value(str), content) == 0);
}

