#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "core.h"
#include "reader.h"
#include "runtime.h"
#include "built_in.h"


int main(int argc, const char** argv) {
    env_t env;
    obj_t* tokens;
    obj_t* parsed;

    if (argc != 2) {
        printf("Please, specify source file\n");
        return 0;
    }

    env = setup_env();

    tokens = tokenize(read_file("lib.lisp"));
    parsed = parse_list(&tokens);
    eval(parsed, env);

    tokens = tokenize(read_file(argv[1]));
    parsed = parse_list(&tokens);
    eval(parsed, env);

    return 0;
}

