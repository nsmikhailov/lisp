#ifndef CTYPES_H
#define CTYPES_H

#include <stdbool.h>
#include <gmp.h>

typedef void anon_t;
typedef void empty_t;
typedef bool bool_t;
typedef char char_t;
typedef double real_t;

#endif /* CTYPES_H */
