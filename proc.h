#ifndef PROC_H
#define PROC_H

#include "ctypes.h"
#include "obj.h"
#include "syntax.h"

typedef obj_t *(*primitive_t)(obj_t *);

obj_t* primitive(primitive_t val);
obj_t* lambda(obj_t* params, obj_t* body);

obj_t* lambda_params(obj_t* lambda);
obj_t* lambda_body(obj_t* lambda);

bool_t is_proc(obj_t* obj);
bool_t is_primitive(obj_t* obj);
bool_t is_lambda(obj_t* obj);

obj_t* apply_primitive(obj_t* proc, obj_t* args);

#include "sym.h"
#define TT_PRIMITIVE (symbol("privmitive"))
#define TT_LAMBDA (symbol(LAMBDA))

#endif /* PROC_H */
