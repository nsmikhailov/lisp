#include <stdlib.h>

#include "proc.h"
#include "typed.h"
#include "pair.h"

obj_t* primitive(primitive_t val) {
    primitive_t *copy = malloc(sizeof(primitive_t));
    *copy = val;
    return typed(TT_PRIMITIVE, (anon_t *)copy);
}

obj_t* lambda(obj_t* params, obj_t* body) {
    return typed(TT_LAMBDA, cons(params, body));
}

obj_t* lambda_params(obj_t* lambda) {
    return car((obj_t *)value_of(lambda));
}

obj_t* lambda_body(obj_t* lambda) {
    return cdr((obj_t *)value_of(lambda));
}

bool_t is_proc(obj_t* obj) {
    return is_primitive(obj) || is_lambda(obj);
}

bool_t is_primitive(obj_t* obj) {
    return is_instance_of(obj, TT_PRIMITIVE);
}

bool_t is_lambda(obj_t* obj) {
    return is_instance_of(obj, TT_LAMBDA);
}

obj_t* apply_primitive(obj_t* proc, obj_t* args) {
    primitive_t func = *((primitive_t *)value_of(proc));
    return (*func)(args);
}

