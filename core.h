#ifndef CORE_H
#define CORE_H

#include "ctypes.h"
#include "obj.h"
#include "sym.h"
#include "pair.h"
#include "typed.h"
#include "number.h"
#include "string.h"
#include "proc.h"
#include "env.h"

#endif /* CORE_H */
