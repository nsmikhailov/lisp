#ifndef TYPED_H
#define TYPED_H

#include "ctypes.h"
#include "obj.h"

obj_t* typed(obj_t* type, anon_t* value);
bool_t is_typed(const obj_t* obj);
bool_t is_instance_of(const obj_t* obj, const obj_t* type);
obj_t* type_of(const obj_t* obj);
anon_t* value_of(const obj_t* obj);

#endif /* TYPED_H */
