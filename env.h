#ifndef ENV_H
#define ENV_H

#include "ctypes.h"
#include "obj.h"

typedef obj_t * env_t;

obj_t* lookup_var_value(obj_t* var, env_t env);
empty_t set_var_value(obj_t* var, obj_t* value, env_t env);
/* env must have at least one frame */
empty_t define_var(obj_t* var, obj_t* val, env_t env);
env_t extend_env(obj_t* variables, obj_t* values, env_t env);

#define the_empty_env (cons(cons(nil, nil), nil))

#endif /* ENVIRONMENT_H */
