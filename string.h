#ifndef STRING_H
#define STRING_H

#include "ctypes.h"
#include "obj.h"

obj_t* string(const char_t* value);
bool_t is_string(obj_t* obj);
char_t* string_value(obj_t* obj);
obj_t* string_copy(obj_t* str);
size_t string_len(obj_t* str);
bool_t is_equal_strings(obj_t* str1, obj_t* str2);
bool_t is_string_content_equal(obj_t* str, const char_t* content);

#include "sym.h"
#define TT_STRING (symbol("string"))

#endif /* STRING_H */
